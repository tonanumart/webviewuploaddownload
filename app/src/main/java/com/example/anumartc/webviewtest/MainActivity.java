package com.example.anumartc.webviewtest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private ValueCallback<Uri[]> valueCallback;
    public ValueCallback<Uri> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE = 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initInstance();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //super.onActivityResult(requestCode, resultCode, intent);
        Log.d("Result","onActivityResult : "+requestCode+" , "+FILECHOOSER_RESULTCODE);
        if(requestCode == FILECHOOSER_RESULTCODE)
        {
            Log.d("Result","File Choose Result");
            if (mUploadMessage == null && valueCallback == null)
                return;

            Uri result = (intent == null || resultCode != RESULT_OK) ? null
                            : intent.getData();
            if(mUploadMessage != null)
            {
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
            if(valueCallback != null){
                valueCallback.onReceiveValue(new Uri[]{
                        result
                });
                valueCallback = null;
            }
        }
    }

    private void initInstance() {
        final WebView webview = findViewById(R.id.webview);
        webview.clearCache(true);
        webview.clearHistory();
        webview.getSettings().setJavaScriptEnabled(true);
        //webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setAllowFileAccess(true);
        webview.getSettings().setAllowContentAccess(true);
        webview.setWebViewClient(new MyWebClient());
        webview.setWebChromeClient(new WebChromeClient(){


            //The undocumented magic method override
            //Eclipse will swear at you if you try to put @Override here
            // For Android 3.0+
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                Log.d("WebViewTest","openFileChooser 3");
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                MainActivity.this.startActivityForResult(Intent.createChooser(i,"File Chooser"), FILECHOOSER_RESULTCODE);

            }

            // For Android 3.0+
            public void openFileChooser( ValueCallback uploadMsg, String acceptType ) {
                Log.d("WebViewTest","openFileChooser 3");
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("*/*");
                MainActivity.this.startActivityForResult(
                        Intent.createChooser(i, "File Browser"),
                        FILECHOOSER_RESULTCODE);
            }

            //For Android 4.1
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture){
                Log.d("WebViewTest","onShowFileChooser 4.1");
                mUploadMessage = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                MainActivity.this.startActivityForResult( Intent.createChooser( i, "File Chooser" )
                        , FILECHOOSER_RESULTCODE );

            }


            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                Log.d("WebViewTest", "onShowFileChooser latest overide");
                MainActivity.this.setValueCallback(filePathCallback);
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                MainActivity.this.startActivityForResult(Intent.createChooser(intent, ""), FILECHOOSER_RESULTCODE);
                return true;
            }

        });


        webview.loadUrl("http://192.168.0.98/Updown");
        Button btn = findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webview.loadUrl("http://192.168.0.98/Updown");
            }
        });
    }

    public void setValueCallback(ValueCallback<Uri[]> valueCallback) {
        this.valueCallback = valueCallback;
    }


    public class MyWebClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.d("MyWebClient","onPageStarted");
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d("MyWebClient","shouldOverrideUrlLoading");
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.d("MyWebClient","onPageFinished");
            super.onPageFinished(view, url);
            //progressBar.setVisibility(View.GONE);
        }
    }
}
